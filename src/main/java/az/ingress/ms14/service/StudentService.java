package az.ingress.ms14.service;

import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service // or we can use @Component
public class StudentService {
//        Dependency Injection type 1
//    @Autowired
//    private StudentRepository studentRepository;

//    Dependency Injection type 2

//    private StudentRepository studentRepository;
//    @Autowired
//    public void setStudentRepository (StudentRepository studentRepository){
//        this.studentRepository = studentRepository;
//    }

//    Dependency Injection type 3
    private final StudentRepository studentRepository;
    public StudentService (StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    public Student get(Integer id) {
        return studentRepository.getById(id);
    }

    public void create(Student student) {
        studentRepository.save(student);
    }

    public void update(Student student) {
        Optional<Student> entity = studentRepository.findById(student.getId());
        entity.ifPresent(student1 ->{
            student1.setName(student.getName());
            student1.setSurname(student.getSurname());
            studentRepository.save(student1);
                });
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public void deleteAll() {
        studentRepository.deleteAll();
    }
}
