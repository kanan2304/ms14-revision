package az.ingress.ms14.controller;

import az.ingress.ms14.model.Register;
import az.ingress.ms14.model.Student;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestMapping("/ms14")
public class GreetingController {
    @PostMapping("/create")
    public String create(@RequestBody Student student){
        return "Hello, " + student.getName();
    }
    @PostMapping("/createRegister")
    public String createRegister(@RequestBody Register register){
        if (!register.getRepeatPassword().equals(register.getPassword())){
            throw new RuntimeException();
        }
        return "User with email " + register.getEmail();
    }

    @GetMapping("/header")
    public String headerSwitch(@RequestHeader("Accept-Language") String lang){
        switch (lang){
            case "en":
                return "hello";
            case "ru":
                return "privet";
            case "az":
                return "salam";
            default:
                return "IDK";
        }
    }
    @GetMapping("/parameter")
    public String paramSwitch(@RequestParam String lang){
        switch (lang){
            case "en":
                return "hello";
            case "ru":
                return "privet";
            case "az":
                return "salam";
            default:
                return "IDK";
        }
    }

}
