package az.ingress.ms14.controller;

import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.StudentRepository;
import az.ingress.ms14.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    // Dependency Injection type 1

//    @Autowired
//    private StudentService studentService;

    //Dependency Injection type 2

//    private StudentService studentService;
//    @Autowired
//    public void setStudentService (StudentService studentService){
//        this.studentService = studentService;
//    }

    //Dependency Injection type 3
    private final StudentService studentService;
    public StudentController (StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/{id}")
    public void get(@PathVariable Integer id){
        studentService.get(id);
    }
    @GetMapping("/all")
    public List<Student> getAll(){
        return studentService.getAll();
    }
    @PostMapping
    public void create(@RequestBody Student student){
        studentService.create(student);
    }
    @PutMapping
    public void update(@RequestBody Student student){
        studentService.update(student);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        studentService.delete(id);
    }
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) ------> another way of writing request url
    @DeleteMapping("/deleteAll")
    public void deleteAll(){
        studentService.deleteAll();
    }
}
